import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class StoreList extends StatelessWidget {
  final List<String> stores = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8.0),
      child: ListView(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        children: stores
            .map((data) => Column(children: [
                  Container(
                      padding: EdgeInsets.all(2.0),
                      child: CircleAvatar(
                        minRadius: 35.0,
                        backgroundColor: Colors.red,
                        child: Text(data,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 10.0,
                            )),
                      ))
                ]))
            .toList(),
      ),
    );
  }
}
