import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import './stories.dart';

class PostList extends StatelessWidget {
  final List<String> posts = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
  ];

  @override
  Widget build(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;
    return ListView.builder(
      itemCount: 5,
      itemBuilder: (context, index) => index == 0
          ? new SizedBox(
              child: new StoreList(),
              height: deviceSize.height * 0.15,
            )
          : Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(16.0, 16.0, 8.0, 16.0),
                  child: Row(
                    children: <Widget>[
                      new Container(
                        child: CircleAvatar(
                          minRadius: 28.0,
                          backgroundColor: Colors.red,
                        ),
                      ),
                      new SizedBox(
                        width: 10.0,
                      ),
                      new Text(
                        "Guilherme Henrique",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                Flexible(
                  fit: FlexFit.loose,
                  child: new Container(
                    height: 280.00,
                    color: Colors.grey,
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Icon(FontAwesomeIcons.heart),
                        new SizedBox(
                          width: 20.0,
                        ),
                        new Icon(FontAwesomeIcons.comment),
                        new SizedBox(
                          width: 20.0,
                        ),
                        new Icon(FontAwesomeIcons.paperPlane),
                        new SizedBox(
                          width: 20.0,
                        ),
                        new Icon(FontAwesomeIcons.bookmark)
                      ],
                    ))
              ],
            ),
    );
  }
}
