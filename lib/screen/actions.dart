import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import './home.dart';
import './gallery.dart';

class Actions extends StatelessWidget {
  final topBar = new AppBar(
    backgroundColor: new Color(0xfff8faf8),
    bottom: TabBar(
      tabs: <Widget>[
        new Text("SEGUINDO",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20.00,
                color: Colors.grey)),
        new Text("VOCÊ",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20.00,
                color: Colors.grey))
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: topBar,
          body: TabBarView(
            children: <Widget>[
              Icon(Icons.directions_car),
              Icon(Icons.directions_transit)
            ],
          ),
          bottomNavigationBar: BottomAppBar(
            shape: CircularNotchedRectangle(),
            notchMargin: 4.0,
            child: new Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  icon: Icon(FontAwesomeIcons.home),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Home()),
                    );
                  },
                ),
                IconButton(
                  icon: Icon(FontAwesomeIcons.search),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Gallery()),
                    );
                  },
                ),
                IconButton(
                  icon: Icon(FontAwesomeIcons.plusSquare),
                  onPressed: () {},
                ),
                IconButton(
                  icon: Icon(FontAwesomeIcons.heart),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Actions()),
                    );
                  },
                ),
                IconButton(
                  icon: Icon(Icons.person),
                  onPressed: () {},
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
